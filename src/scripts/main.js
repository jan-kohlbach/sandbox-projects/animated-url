(() => {
  const loop1 = () => {
    let n;
    let s = '';

    for (let i = 10; i > 0; i -= 1) {
      n = Math.floor(Math.sin((Date.now() / 200) + (i / 2)) * 4) + 4;
      s += String.fromCharCode(0x2581 + n);
    }

    window.location.hash = s;
  };

  setInterval(loop1, 50);


  const array = ['🕐', '🕑', '🕒', '🕓', '🕔', '🕕', '🕖', '🕗', '🕘', '🕙', '🕚', '🕛'];
  const loop2 = () => {
    window.location.hash = array[Math.floor((Date.now() / 100) % array.length)];
  };

  // setInterval(loop2, 50);
})();
